package com.example.trpobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrpoBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrpoBackendApplication.class, args);
    }

}
